import { Component, trigger, transition, style, state, animate, ViewChild, keyframes } from '@angular/core';
import { NavController, Slides, Slide,  } from 'ionic-angular';
import { MainPage } from '../main/main';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
    
  trigger('jump', [
        state('*', style({
            transform: 'translateX(0)'
        })),
        transition('* => swipedRight', animate('700ms ease-out', keyframes([
          style({transform: 'translateX(0)', offset: 0}),
          style({transform: 'translateX(-85px)',  offset: 0.4}),
          style({transform: 'translateX(0)',     offset: 1.0})
        ]))),
        transition('* => swipedLeft', animate('700ms ease-out', keyframes([
          style({transform: 'translateX(0)', offset: 0}),
          style({transform: 'translateX(85px)',  offset: 0.4}),
          style({transform: 'translateX(0)',     offset: 1.0})
        ])))
    ])
  ]
})
export class HomePage {
  @ViewChild(Slides) slides: Slides
  heading: string = 'My Awesome Heading';
  paragraph: string = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus nemo asperiores odit molestias!';
  skipMsg: string = 'Next';
  state: string = 'x';
 

  constructor(public navCtrl: NavController) {

  }
  skip() {
    this.navCtrl.push(MainPage);
  }
  slideChanged() {
    if (this.slides.isEnd())
      this.skipMsg = 'Alright, Move On!';
  }
  slideMoved() {
    if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex()) 
      this.state = 'swipedRight';
    else 
      this.state = 'swipedLeft';
  }

}
